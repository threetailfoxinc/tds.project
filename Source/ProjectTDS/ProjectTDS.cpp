// Copyright Epic Games, Inc. All Rights Reserved.

#include "ProjectTDS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ProjectTDS, "ProjectTDS" );

DEFINE_LOG_CATEGORY(LogProjectTDS)
 