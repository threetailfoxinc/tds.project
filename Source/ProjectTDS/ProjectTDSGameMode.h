// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjectTDSGameMode.generated.h"

UCLASS(minimalapi)
class AProjectTDSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AProjectTDSGameMode();
};



