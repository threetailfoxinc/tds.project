// Copyright Epic Games, Inc. All Rights Reserved.

#include "ProjectTDSGameMode.h"
#include "ProjectTDSPlayerController.h"
#include "ProjectTDSCharacter.h"
#include "UObject/ConstructorHelpers.h"

AProjectTDSGameMode::AProjectTDSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AProjectTDSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("Blueprint'/Game/Blueprint/Character/TopDownCharacter.TopDownCharacter'"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}